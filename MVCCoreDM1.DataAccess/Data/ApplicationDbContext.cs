﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MVCCoreDM1.Models;

namespace MVCCoreDM1.DataAccess.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        #region Properties
        public DbSet<Tenant> Tenants { get; set; }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        #endregion

        #region CTOR
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
          : base(options)
        {
        } 
        #endregion
    }
}
