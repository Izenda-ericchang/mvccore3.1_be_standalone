﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MVCCoreDM1.DataAccess.Migrations
{
    public partial class updateUserTenantIdNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_Tenant_Id",
                table: "AspNetUsers",
                column: "Tenant_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Tenants_Tenant_Id",
                table: "AspNetUsers",
                column: "Tenant_Id",
                principalTable: "Tenants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Tenants_Tenant_Id",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_Tenant_Id",
                table: "AspNetUsers");
        }
    }
}
