﻿using MVCCoreDM1.DataAccess.Data;
using MVCCoreDM1.DataAccess.Repository.IRepository;
using MVCCoreDM1.Models;
using System.Linq;

namespace MVCCoreDM1.DataAccess.Repository
{
    public class TenantRepository : Repository<Tenant>, ITenantRepository
    {
        #region Variables
        private readonly ApplicationDbContext _db;
        #endregion

        #region CTOR
        public TenantRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
        #endregion

        #region Methods
        public void Update(Tenant tenant)
        {
            var objFromDb = _db.Tenants.FirstOrDefault(t => t.Id == tenant.Id);

            if (objFromDb != null)
            {
                objFromDb.Name = tenant.Name;
            }
        }
        #endregion
    }
}
