﻿using MVCCoreDM1.Models;

namespace MVCCoreDM1.DataAccess.Repository.IRepository
{
    public interface ITenantRepository : IRepository<Tenant>
    {
        #region Methods
        void Update(Tenant tenant);
        #endregion
    }
}
