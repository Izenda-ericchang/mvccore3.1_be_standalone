﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(MVCCoreDM1.Areas.Identity.IdentityHostingStartup))]
namespace MVCCoreDM1.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}
