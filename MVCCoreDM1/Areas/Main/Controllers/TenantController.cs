﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MVCCoreDM1.DataAccess.Repository.IRepository;
using MVCCoreDM1.IzendaBoundary;
using MVCCoreDM1.Models.IzendaModel;
using MVCCoreDM1.Models.ViewModels;
using MVCCoreDM1.Utility;
using System.Threading.Tasks;

namespace MVCCoreDM1.Areas.Main.Controllers
{
    [Area("Main")]
    [Authorize(Roles = SD.Role_Admin)]
    public class TenantController : Controller
    {
        #region Variables
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region CTOR
        public TenantController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        // GET: Main/Tenant
        public async Task<IActionResult> Index()
        {
            var tenantList = await _unitOfWork.Tenant.GetAllAsync();

            return View(tenantList);
        }

        // GET: Main/Tenant/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var tenant = await _unitOfWork.Tenant.GetFirstOrDefaultAsync(t => t.Id == id);
            if (tenant == null)
                return NotFound();

            var izendaTenant = await GetIzendaTenantDetail(tenant);

            // TODO: retrieve tenant name from Izenda Config
            var tenantVM = new TenantVM()
            {
                Tenant = tenant,
                TenantDetailName = izendaTenant.Name
            };

            return View(tenantVM);
        }

        private static async Task<TenantDetail> GetIzendaTenantDetail(Models.Tenant tenant)
        {
            var izendaAdminToken = IzendaTokenAuthorization.GetIzendaAdminToken();
            var izendaTenant = await IzendaUtilities.GetIzendaTenantByName(tenant.Name, izendaAdminToken);
            return izendaTenant;
        }

        // GET: Main/Tenant/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Main/Tenants/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TenantVM tenantVM)
        {
            if (ModelState.IsValid)
            {
                // check user DB first
                var isTenantExisting = _unitOfWork.Tenant.GetFirstOrDefault(t => t.Name.Equals(tenantVM.Tenant.Name));

                if (isTenantExisting == null)
                {
                    var izendaAdminToken = IzendaTokenAuthorization.GetIzendaAdminToken();
                    // try to create a new tenant at izenda DB
                    var success = await IzendaUtilities.CreateTenant(tenantVM.Tenant.Name, tenantVM.TenantDetailName,  izendaAdminToken);

                    if (success)
                    {
                        _unitOfWork.Tenant.Add(tenantVM.Tenant);
                        await _unitOfWork.SaveAsync();

                        return RedirectToAction(nameof(Index));
                    }
                    ModelState.AddModelError(string.Empty, "Failed to create a new tenant. Tenant already exists in DB.");
                }
                else
                    ModelState.AddModelError(string.Empty, "Failed to create a new tenant. Tenant already exists in DB.");
            }
            return View(tenantVM);
        }

        private async Task<bool> TenantExists(int id)
        {
            var tenant = await _unitOfWork.Tenant.GetFirstOrDefaultAsync(t => t.Id == id);

            return tenant != null;
        }
        #endregion
    }
}
