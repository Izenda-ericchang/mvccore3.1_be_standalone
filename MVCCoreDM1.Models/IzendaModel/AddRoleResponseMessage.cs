﻿namespace MVCCoreDM1.Models.IzendaModel
{
    public class AddRoleResponeMessage
    {
        #region Properties
        public bool Success { get; set; }

        public RoleDetail Role { get; set; }
        #endregion
    }
}
